"""Netcdf correction module.

/!\ THIS VERSION IS FOR SEA-LEVEL HUR/HURS CORRECTION ONLY

Assumes all files have the same grid.
For instance re-align data with cdo :
$ cdo griddes reference.nc > grid.txt
$ cdo remapcon,grid.txt old-file.nc new-file.nc

Each model file must be stored into INDIR/historical/ and INDIR/'scenario'/

The main function is then
>>> correct_nc(param_name, reference_file, historical_file, output_directory)
where param_name is the netcdf variable name.
The function looks for available scenarii in INDIR.

For simplicity, the first scenario will contain historical time period
as well, and will be stored into OUTDIR/historical/
directory. Subsequent scenarii will only contain scenario period
(i.e. starting at SCENYR) and will be stored into OUTDIR/'scenario'/ .
"""

from correct import *
import sys
import os
import time

SCEN_LIST = ('ssp126', 'ssp245', 'ssp585')
# SCEN_LIST = ('rcp26', 'rcp45', 'rcp85')
HIST_NAME = 'historical'
# TH = 8.64e-2 # per day
# TH = 1e-6 # per s
TH = 1e-6

def copy_ds(filename, src, omit=('history')):
    """Opens a new netcdf file for writing and copies all metadata found
    on src, except hidden attributes.
    Dataset is not closed after execution and must be closed manually by user.
    """
    dst = Dataset(filename, mode='w') 

    for a in src.ncattrs():
        if a not in omit:
            dst.setncattr(a, src.getncattr(a))

    for name, dimension in src.dimensions.items():
        if name == 'plev':
            dst.createDimension(name, 1)
        elif dimension.isunlimited():
            dst.createDimension(name, None)
        else:
            dst.createDimension(name, dimension.size)
        
    for name, variable in src.variables.items():
        x = dst.createVariable(name, variable.datatype, variable.dimensions)
        for a in variable.ncattrs():
            if a[0] != '_': # not a hidden attrs
                x.setncattr(a, variable.getncattr(a))
        if x.name in ('lon', 'lat', 'time') :
            dst.variables[x.name][:] = src.variables[x.name][:]
        elif x.name == 'plev':
            dst.variables[x.name][:] = np.array([ 100000 ])

    return dst # don't close


def grid_correct(param, reffile, rcpfile, scenario, outfile=None, has_hist=True, save_hist=True):
    """Main management function. Corrects a single file (possibly with historical).

    param    : parameter (str)
    reffile  : reference nc file
    rcpfile  : projection nc file
    scenario : scenario name (str)
    has_hist : concatenate to historical file
    save_hist: save historical period as well (assumes has_hist)
    """
    t0 = time.time()
    print(rcpfile)
    save_hist = save_hist and has_hist

    print('Loading times')
    refds = Dataset(reffile)
    reftr = TimeRange(refds)
    rcpds = Dataset(rcpfile)

    # builds history
    if has_hist:
        histfile = rcpfile.replace(scenario, HIST_NAME)
        histds = Dataset(histfile)
        projtr = TimeRange(histds)
        rcptr = TimeRange(rcpds)
        projtr.concat(rcptr)
        scen_yr = rcptr.year[0]
    else:
        projtr = TimeRange(rcpds)

    # starting prediction year
    startyr = scen_yr if (has_hist and not save_hist) else projtr.year[0]
    idxs = indexes(reftr, projtr, start=startyr)

    print('Opening new file')
    # sizes (lon, lat, time)
    nx, ny = refds['lon'].shape[0], refds['lat'].shape[0]
    nt = (projtr.year >= startyr).sum()
    # data buffer :
    # latitude slices are the best tradeoff between performance and memory consumption
    buf = np.zeros((nt, nx), dtype=np.float32)

    rcp_outfile = outfile or os.path.join(outdir, scenario, os.path.basename(rcpfile))
    rcp_dst = copy_ds(rcp_outfile, refds)
    if save_hist:
        # when saving history, selects the 'historical' dir
        hist_outfile = os.path.join(outdir, 'historical', os.path.basename(histfile))
        hist_dst = copy_ds(hist_outfile, refds)

    if has_hist:
        # get cutting index
        icut = projtr.cut(scen_yr)

    print('Starting pixel iteration')
    for y in range(ny):
        # store horizontal slice into buffer
        for x in range(nx):
            if has_hist:
                proj = np.hstack((histds[param][:,0,y,x], rcpds[param][:,0,y,x]))
            else :
                proj = rcpds[param][:,0,y,x]

            # compute !
            a = window_correct(refds[param][:,y,x], proj, idxs, pr=(param=='hur'), th=TH, windowed=True, verbose=False)
            buf[:,x] = a[icut:] if has_hist and not save_hist else a
            
            print('.', end='', flush=True)
        print(y)
        # save the slice in netcdf form
        if save_hist:
            hist_dst[param][:,y,:] = buf[:icut,:]
            rcp_dst[param][:,y,:] = buf[icut:,:]
        else:
            rcp_dst[param][:,y,:] = buf[:,:]
    print('')

    rcp_dst.close()
    refds.close()
    if has_hist:
        histds.close()
        if save_hist:
            hist_dst.close()
    rcpds.close()
    print(f"Scenario {scenario} calculated in {int(time.time()-t0)}s")

    
def by_hist(param, reffile, histfile, outdir):
    for d in SCEN_LIST + (HIST_NAME,):
        os.makedirs(os.path.join(outdir, d), exist_ok=True)
        
    save_hist = True
    for s in SCEN_LIST:
        fs = histfile.replace(HIST_NAME, s)
        if not os.path.exists(fs):
            continue
        grid_correct(param, reffile, fs, s, has_hist=True, save_hist=save_hist)
        # save history only once
        save_hist = False
            
                    
def usage():
    print(f'Usage : \n    {sys.argv[0]} ref.nc historical.nc param outdir (for several files sharing the same history)\
\nor  {sys.argv[0]} --single ref.nc file.nc param outfile.nc   (for a single file)')
    exit(1)
        
if __name__ == '__main__':
    if len(sys.argv) == 6 and sys.argv[1] == '--single':
        reffile, rcpfile, param, outfile = sys.argv[2:]
        grid_correct(param, reffile, rcpfile, scenario=None, outfile=outfile, has_hist=False, save_hist=False)
    elif len(sys.argv) == 5:
        reffile, histfile, param, outdir = sys.argv[1:]
        by_hist(param, reffile, histfile, outdir)
    else: 
        usage()
