from correct import *
from calendar import monthrange
import os
import sys

SCENLIST = ('rcp26', 'rcp45', 'rcp85')
# S_IN_DAY = 86400  # for precip
TH = 8.64e-2 # mm per day


def xls2pd(xlsfile):
    """Reads an xls (or xlsx) file with for instance the following formatting :
    station_id, lon, lat, year, month, [1..31]
    """
    l = []
    df = pd.read_excel(xlsfile)
    lon, lat = df.loc[0].iloc[1], df.loc[0].iloc[2]
    yrcol, moncol, daycol = 3, 4, 5

    for _, row in df.iterrows():
        # non-numeric items are interpreted as nans
        row = pd.to_numeric(row, errors='coerce')
        if row.iloc[3:6].isna().any():
            continue
        yr, mon = int(row.iloc[yrcol]), int(row.iloc[moncol])
        _, last = monthrange(yr, mon) # last day of month
        l.append(pd.Series(row.iloc[daycol:daycol+last].to_numpy(), dtype=np.float32,
                           index = pd.date_range(start='{}-{}-01'.format(yr,mon), end='{}-{}-{}'.format(yr,mon,last))))
    res = pd.concat(l).sort_index()
    # remove nans
    return res[~res.isna()], lon, lat


def getlonlat(val, ds, lonlat):
    """Value -> array index.
    lonlat: usually 'lat' or 'lon'
    """
    ds0 = ds[lonlat][0]
    res = ds[lonlat][1] - ds0
    if val < ds0 - res / 2:
        val += 360
    r = int(round((val - ds0) / res))
    return r        


def station_correct(ref, reftr, ncfile, lon, lat, param):
    """Extracts values of a given point from a netcdf file.
    Returns a pandas Series with model name."""
    
    with Dataset(ncfile) as ds:        
        projtr = TimeRange(ds)        
        xlon = getlonlat(lon, ds, 'lon')
        ylat = getlonlat(lat, ds, 'lat')
        proj = ds[param][:,ylat,xlon] 
        name = ds.model_id

    try:
        pdidx = projtr.to_pd_idx() 
    except IndexError as e: 
        print(f'  {e} Removing impossible dates.')
        proj = proj[projtr.trim()]
        pdidx = projtr.to_pd_idx()
                
    idxs = indexes(reftr, projtr)
    corr = window_correct(ref, proj, idxs, pr = (param=='pr'), verbose=False, th=TH)

    return pd.Series(corr, index=pdidx, name=name)


def station_correct_all_nc(reffile, ncdir, param):

    print(f'Reading reference file {reffile}.')
    _, fext = os.path.splitext(reffile)
    if fext.startswith('.xls'):
        ref, lon, lat = xls2pd(reffile)
    else:
        # TODO
        ref = pd.read_csv(reffile, index_col=0, parse_dates=True)
        ref = ref[ref.columns[0]] # convert to Series
        lon = -12
        lat = 16

    ref = ref['1950':]
    l = []
    
    reftr = PdTimeRange(ref)
    ref = ref.to_numpy()
    
    for f in os.scandir(ncdir):
        print(f.path)
        l.append(station_correct(ref, reftr, f.path, lon, lat, param))            

    return pd.concat(l, axis=1)

def station_correct_all(refdir, ncdir, param, outdir):
    """All reference files, all scenarios."""
    for s in SCENLIST:
        os.makedirs(os.path.join(outdir, s), exist_ok=True)
        for reffile in os.scandir(refdir):
            base, ext = os.path.splitext(reffile.name)
            if ext == '.xls':
                outfile = os.path.join(outdir, s, base + '.csv')
                df = station_correct_all_nc(reffile.path, os.path.join(ncdir, s), param)
                df.to_csv(outfile)

            
if __name__ == '__main__':
    if len(sys.argv) != 6:
        print(f'Usage: \
\n    {sys.argv[0]} --file reffile.[csv/xls] ncdir param out.csv (one station file)\
\nor  {sys.argv[0]} --dir refdir ncdir param outdir             (many stations)')
        exit(1)

    if sys.argv[1] == '--file':
        reffile, ncdir, param, outfile = sys.argv[2:]    
        df = station_correct_all_nc(reffile, ncdir, param)
        df.to_csv(outfile)
    else:
        refdir, ncdir, param, outdir = sys.argv[2:]    
        station_correct_all(refdir, ncdir, param, outdir)
        
