
# station/grid_correct

Climate model correction by reference periods (local observations or
gridded models) using CDF-t method. 

## Installation 

Requires python > 3.6.

 - Main dependencies :
  ```bash
  $ pip install netCDF4 numpy pandas cython matplotlib scipy sklearn
  ```
 - We also need [SBCK](https://github.com/yrobink/SBCK) :
  ```bash
  $ git clone https://github.com/yrobink/SBCK.git
  $ cd SBCK/python
  $ python3 setup.py install
  ```

## Usage

### station correction

Cross-corrects a set of (gridded, in netcdf format) climate model
projections with a set of (point, in xls format) local observations.

#### one station only

```bash
$ python3 station_correct.py --file ref.xls ncdir param out.csv
```
where
 - ref.xls : observation file in xls format
 - ncdir : raw models, stored under scenario subdirectories
   (by default named rcp26, rcp45 and rcp85).
 - param : variable (netcdf) name
 - out.csv : output file

#### many stations

Returns one file per point.

```bash
$ python3 station_correct.py --dir refdir ncdir param outdir
```

where
 - refdir : directory of observation files in xls format
 - ncdir : raw models, stored under scenario subdirectories
   (by default named rcp26, rcp45 and rcp85).
 - param : variable (netcdf) name
 - outdir : output directory (will be created if does not exist)



### grid correction 

For easier job management, only one model is processed at a time. Raw
model netcdf files are stored under subdirectories of `ncdir` labeled
by scenario (and optionally "historical").

There are two modes depending if there is a shared "historical" period
or not.

#### by model and scenario

The simplest way is to correct independently each file.

```bash
$ python3 grid_correct.py --single ref.nc proj.nc param out.nc
```

#### by model only

If several projection scenarios share a common historical part, the
latter is probably stored in a separate file. It is faster and more
convenient to calculate all scenarios for a given model in one go. In
that case, we provide the common historical file path.

The program will spit out as much files as scenarios found (plus
historical) in `outdir/`.

```bash
$ python3 grid_correct.py ref.nc historical.nc param outdir
```


## Bibliography

 - *A bias-corrected CMIP5 dataset for Africa using the CDF-t method –
  a contribution to agricultural impact studies* (Adjoua Moise Famien,
  Serge Janicot, Abe Delfin Ochou, Mathieu Vrac, Dimitri Defrance,
  Benjamin Sultan, and Thomas Noël),
  https://doi.org/10.5194/esd-9-313-2018

 - *Bias correction of precipitation through Singularity Stochastic
   Removal: Because occurrences matter* (Mathieu Vrac, Thomas Noël and
   Robert Vautard), https://doi.org/10.1002/2015JD024511
   
