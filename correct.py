"""Correction method for a single series.

Usage : 
first extract the time indexes, using TimeRange objects : 

>>> reftr = TimeRange(refds) # for netCDF4 Dataset
or 
>>> reftr = PdTimeRange(refdf) # for pandas Dataframe
and same for projtr.

then 

>>> idxs = indexes(reftr, projtr)

You may now plug these indexes into the main function. The idea is to 
compute indexes once for all future projections (esp. useful for netcdf).

>>> corr = window_correct(ref, proj, idxs, ...)
"""

from netCDF4 import Dataset, num2date, date2num
from SBCK import CDFt, OTC
import numpy as np
import pandas as pd

# SSR threshold
# TH = 8.64e-2 # mm per day
TH = 1e-6  # mm per s
TIME_UNITS = 'days since 1950-01-01 00:00:00' # set to None to keep existing units


class TimeRange:
    """Human-readable dates extracted from netcdf time variable. 

    @param ds:netCDF4.Dataset
    """
    
    def __init__(self, ds, timename='time'):
        self.cal = ds[timename].calendar
        self.uni = ds[timename].units if TIME_UNITS is None else TIME_UNITS
        self.dates = num2date(list(ds[timename][:]), ds[timename].units, self.cal)
        self.year = np.array([x.year for x in self.dates])
        self.month = np.array([x.month for x in  self.dates])
        self.len = self.dates.size

    def __repr__(self):
        return f'TimeRange [{self.year[0]}-{self.year[-1]}] {self.cal}'

    def __len__(self):
        return self.len

    def cut(self, yr):
        """First index of yr."""
        return np.nonzero(self.year>=yr)[0][0]
    
    def concat(self, other):
        """Concatenates to another TimeRange, keeping original uni and cal."""
        self.year = np.hstack((self.year, other.year))
        self.month = np.hstack((self.month, other.month))
        self.dates = np.hstack((self.dates, other.dates))
        self.len += other.len

    def to_num(self, startyr=None, endyr=None):
        """Exports to netcdf numerical time system."""
        if startyr or endyr:
            mask = (self.year >= startyr) if startyr else (self.year < endyr)
            return date2num(self.dates[mask], self.uni, self.cal)
        return date2num(self.dates, self.uni, self.cal)

    def trim(self):
        """Removes februarys 29 and 30. Assumes 360_ or 366_day calendar.

        366_day turns into 365_day ; 
        360_day into ... something else, named 365_day for convenience."""
        # technically, 366_day could be turned into gregorian
        # but 360_day is a mess anyway
        day = np.array([x.day for x in self.dates])
        real = ~((self.month == 2) & ((day == 29) | (day == 30)))
        self.dates = self.dates[real]
        self.year = self.year[real]
        self.month = self.month[real]
        self.cal = '365_day'
        self.len = real.sum()
        return real
    
    def to_pd_idx(self):
        """Exports as pandas time index if possible.
        If not, raises IndexError. See trim()."""
        if self.cal=='360_day' or self.cal=='366_day':
            raise IndexError(f'Calendar {self.cal} not supported.')        
        return [ pd.Timestamp(x.strftime('%Y-%m-%d')) for x in self.dates ]
        

class PdTimeRange(TimeRange):
    """A TimeRange extracted from a pandas Series or Dataframe (takes index)."""
    
    def __init__(self, df):
        self.cal = 'standard'
        self.uni = TIME_UNITS
        self.dates = df.index.to_pydatetime()
        self.year = df.index.year
        self.month = df.index.month
        self.len = len(df)

    
def indexes(reftr, projtr, start=None, verbose=False, maxyrrange=None):
    """Builds the mappings used for calculations.

    Returns a list of tuples with len 12 (each month). Each 'index' is a boolean list.
    One tuple is composed of
    0. index of reference (in bool array form)
    1. index of projection
    2. index of reference part of projection (same length as ref)
    3. list of windows [(w1, w2), ...] where w2 is a subset of w1, 
        w1 being the fitting window,
        w2 the prediction.
    """

    # fitting interval (max 100 yrs)
    yrmax = min(projtr.year[-1], reftr.year[-1])
    maxyrrange = maxyrrange or 100
    yrmin = max(projtr.year[0], reftr.year[0], yrmax-maxyrrange)
    
    res = []
    for month in range(1,13):
        monrefidx = (reftr.month == month) & (reftr.year >= yrmin) & (reftr.year <= yrmax)
        monprojidx = (projtr.month == month)
        monprojrefidx = monprojidx & (projtr.year >= yrmin) & (projtr.year <= yrmax)

        # ensure that monrefidx and monprojredidx have same size
        rsum, prsum = monrefidx.sum(), monprojrefidx.sum()
        if rsum != prsum :
            if prsum == 0:
                raise IndexError('No calibration segment.')
            # calibration segments should have the same size :
            # remove indexes from the largest, at random
            rem = monrefidx if rsum > prsum else monprojrefidx
            remi = rem.nonzero()[0]
            rem[np.random.choice(remi, abs(rsum-prsum))] = False            

        windows = [] # [(large window 1, narrow window 1), ... ]
        if start is None:
            start = projtr.year[0]
        end = projtr.year[-1]
        wstart = i = start
        # fitting and correction windows (resp w1 and w2) :
        # +------- w1 --------+
        # |    +-- w2 ---+    |
        # |----|---------|----|
        #   4       9      4
        # except for starting and ending windows, where w2 sticks to the side.
        while i + 17 < end:
            if verbose and month == 1:
                print(f'[{i} [{wstart} - {i+12}] {i+16}]')
            windows.append((monprojidx & (projtr.year >= i) & (projtr.year < i+17),       # large window
                            monprojidx & (projtr.year >= wstart) & (projtr.year < i+13))) # small window
            i += 9
            wstart = i + 4
        # and the last window
        if verbose and month == 1:
            print(f'[{i} [{wstart} - {end}] {end}]')
        windows.append((monprojidx & (projtr.year >= i),
                        monprojidx & (projtr.year >= wstart)))

        res.append((monrefidx, monprojidx, monprojrefidx, windows))
    return res        


def ssr(data, th=TH):
    """Applies SSR method in-place : all data < TH is mapped to a uniform
    distribution on [0, TH]."""
    inf = data<th
    data[inf] = np.random.uniform(high=th, size=inf.sum())


def invssr(data, th=TH):
    """Reverse of SSR method : all data < TH is set to zero."""
    data[data<th] = 0
    
def binwidth(ref, proj):
    """Freeman-Draconis rule.

    See
    https://en.wikipedia.org/wiki/Freedman%E2%80%93Diaconis_rule
    """
    # print(f'ref shape : {ref.shape[0]}')
    # 2. * ( np.percentile( X , q = 75 , axis = 0 ) - np.percentile( X , q = 25 , axis = 0 ) ) / np.power( X.shape[0] , 1. / 3. )
    # nh = np.log2( ref.shape[0] ) + 1.
    nh = np.power(ref.shape[0], 1./3.)
    xmin = min(np.percentile(ref, q=25), np.percentile(proj, q=25))
    xmax = max(np.percentile(ref, q=75), np.percentile(proj, q=75))
    return ((xmax-xmin) / (2*nh), )

def window_correct(ref, proj, idxs, verbose=False, pr=True, windowed=True, th=TH):
    """Applies moving window correction on projection.

    ref, proj : arrays (will be modified)
    idxs      : see indexes()
    pr        : when correcting precipitation, some special rules apply.
    windowed  : apply rolling correction. If False, corrects everything in one pass (thus ignoring idxs[3]).
    th        : if pr, minimum threshold value. 
                For instance 8.64e-2 (when correcting mm per day) or 1e-6 (mm per s).

    """

    # default values are original projections
    result = proj.copy()

    if pr:
        # map to uniform distribution for small values
        ssr(ref, th)
        ssr(proj, th)
        
    # select the same bin width for all months
    # (automatic bin_width calculator may be problematic in precipitation case)
    bin_width = binwidth(ref, proj) if pr else None
    
    for month in range(12):
        if verbose:
            print(f'Month {month+1}')
        monrefidx = idxs[month][0]
        monref = ref[monrefidx]
        monprojidx = idxs[month][1]
        monprojref = proj[ idxs[month][2] ]
        
        if pr:
            pnz = monprojref > th  
            rnz = monref > th         
            if not (pnz.any() and rnz.any()) :
                # values are too small :
                # skip this month's correction (use uncorrected values)
                if verbose:  
                    print(f'  M{month+1} : too low, correction abandoned')
                continue
            
            # reproject precipitations
            refcumul, projrefcumul = monref.sum(), monprojref.sum()
            if refcumul < 2*projrefcumul:
                proj[monprojidx] *= (refcumul / projrefcumul) 
                monprojref = proj[ idxs[month][2] ]
                
        else:  # temperature and other variables
            # realign to reference mean
            proj[monprojidx] += monref.mean() - monprojref.mean()                        
            # re-extract calibration segment 
            monprojref = proj[ idxs[month][2] ]
            
        cdft = CDFt(bin_width=bin_width)
        # see also :
        # cdft = OTC()
        
        try : 
            if windowed :
                # default behaviour : sequence of overlapping windows
                windows = idxs[month][3]
            else:            
                # correct everything at once with a single window
                windows = ( (monprojidx, monprojidx ), )
                
            # start moving window
            for w1,w2 in windows:
                cdft.fit(monref, monprojref, proj[w1])
                predict = cdft.predict(proj[w2]).flatten()
                if pr:
                    # realign to zero :
                    # uniform map values from [ min, min+th ] to [ 0, min+th ]
                    predictmin = np.min(predict) + th
                    # print(f'{predictmin*86400:.2f} ', end='')
                    predictzero = predict <= predictmin
                    predict[predictzero] = np.random.uniform(high=predictmin, size=predictzero.sum())                
                result[w2] = predict
            # print('')
                    
        except (MemoryError, ValueError) as e:
            if verbose:
                print(f'  M{month+1} : {e}')
            continue
        
    if pr:
        # re-align near-zeroes to zero
        invssr(result, th)
        
    return result

